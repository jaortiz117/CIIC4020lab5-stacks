package classes;

public class SymmetricStringAnalyzer {
	private String s; 
	
	public SymmetricStringAnalyzer(String s) {
		this.s = s; 
	}
	
	/**
	 * Determines if the string s is symmetric
	 * @return true if it is; false, otherwise. 
	 */
	public boolean isValidContent() { 
		SLLStack <Character> stack = new SLLStack<Character>();

		for (int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			if(Character.isLetter(c)){
				if(Character.isUpperCase(c))
					stack.push(c);
				else if(stack.isEmpty())
					return false;
				else{
					char t = stack.top();
					if(t==Character.toUpperCase(c))
						stack.pop();
					else
						return false;

				}
			}
			else
				return false;
		}

		return stack.isEmpty();  
	}
	
	/**
	 * returns reference to internal string
	 */
	public String toString() { 
		return s; 
	}

	/**
	 *  If “s” is a valid symmetric String, this method returns 
	 *  a parenthesized expression from the content in “s”.
	 *  Such expression is one that contains all the elements
	 *  in the string, but where each symmetric substring in it
	 *  is enclosed within < and >.
	 * @return string enclosed in <> as brackets
	 * @throws StringIsNotSymmetricException
	 */
	public String parenthesizedExpression() 
	throws StringIsNotSymmetricException 
	{
		if(!this.isValidContent())
			throw new StringIsNotSymmetricException(" The string is not a valid symmetric string.");
		
		String newString = "";
		
		for (int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			
			if(Character.isUpperCase(c)){
				newString = newString + " <" + c + " ";
			}
			else
				newString = newString + " " + c + "> ";
			
			
		}
		return newString;
	}

}
