package menuClasses;

import java.util.ArrayList;

public class AlterListContentMenu extends Menu{

	private static AlterListContentMenu ACM = new AlterListContentMenu(); 
	private AlterListContentMenu() { 
		super(); 
		String title; 
		ArrayList<Option> options = new ArrayList<Option>();  
		title = "Alter List Content"; 
		options.add(new Option("Add a new value to List", new AddToListAction())); 
		options.add(new Option("Delete a Position from a List", new DeleteFromListAction())); 
		options.add(new Option("Show Content of a List", new ShowListAction()));
		options.add(Option.EXIT); 

		super.InitializeMenu(title, options); 

	}
	
	public static AlterListContentMenu getAlterListsMenu() { 
		return ACM; 
	}
	
}
