package menuClasses;

import dataManager.DMComponent;
import ioManagementClasses.IOComponent;

public class DeleteListAction implements Action {

	@Override
	public void execute(Object args) {

		DMComponent dmc = (DMComponent)args;
		IOComponent io = IOComponent.getComponent(); 
		io.output("\nRemoving Selected List from system:\n");
		String listName = io.getInput("\nEnter name of list to remove: "); 
		dmc.removeList(listName);
	}

}
